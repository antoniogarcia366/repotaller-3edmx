let productsObteined;

function getProducts() {
  //Tech U
  /*let uri = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  let request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    if (this.readyState == 4 && this.status == 200) {
      //console.table(JSON.parse(request.responseText).value);
      productsObteined = request.responseText;
      procesarProductos();
    }
  }
  request.open("GET",uri, true);
  request.send();*/

  fetch('https://services.odata.org/V4/Northwind/Northwind.svc/Products')
      .then(function (response) {
          /*console.log('response.body = ', response.body);
          console.log('response.bodyUsed = ', response.bodyUsed);
          console.log('response.headers = ', response.headers);
          console.log('response.ok = ', response.ok);
          console.log('response.status = ', response.status);
          console.log('response.statusText = ', response.statusText);
          console.log('response.type = ', response.type);
          console.log('response.url = ', response.url);*/

          return response.text();
      }).then(function (data) {
          //console.log('data = ', data);
          //console.table(JSON.parse(data));
          productsObteined = data;
          procesarProductos();
      }).catch(function (err) {
          console.log(err);
      });

}

function procesarProductos() {

  let JSONProducts = JSON.parse(productsObteined);
  //alert(JSONProductos.value[0].ProductName);
  let divTableProducts = document.getElementById("divTableProducts");
  let tableProducts    = document.createElement("table");
  let tbody            = document.createElement("tbody");

  tableProducts.classList.add("table");
  tableProducts.classList.add("table-striped");

  for (var i = 0; i < JSONProducts.value.length; i++) {

    let newRow = document.createElement("tr");

    let columnName = document.createElement("td");
    columnName.innerText = JSONProducts.value[i].ProductName;

    let columnPrice = document.createElement("td");
    columnPrice.innerText = JSONProducts.value[i].UnitPrice;

    let columnStock = document.createElement("td");
    columnStock.innerText = JSONProducts.value[i].UnitsInStock;

    newRow.appendChild(columnName);
    newRow.appendChild(columnPrice);
    newRow.appendChild(columnStock);

    tbody.appendChild(newRow);

  }

  tableProducts.appendChild(tbody);
  divTableProducts.appendChild(tableProducts);



}

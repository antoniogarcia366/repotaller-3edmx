let customersObteined;

function getCustomers() {

  //Tech U
  /*let uri = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter = Country eq 'Germany'";
  let request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    if (this.readyState == 4 && this.status == 200) {
      console.table(JSON.parse(request.responseText).value);
    }
  }
  request.open("GET",uri, true);
  request.send();*/


  fetch("https://services.odata.org/V4/Northwind/Northwind.svc/Customers")
      .then(function (response) {
          /*console.log('response.body = ', response.body);
          console.log('response.bodyUsed = ', response.bodyUsed);
          console.log('response.headers = ', response.headers);
          console.log('response.ok = ', response.ok);
          console.log('response.status = ', response.status);
          console.log('response.statusText = ', response.statusText);
          console.log('response.type = ', response.type);
          console.log('response.url = ', response.url);*/

          return response.text();
      }).then(function (data) {
          //console.log('data = ', data);
          //console.table(JSON.parse(data));

          customersObteined = data;
          processCustomers();

      }).catch(function (err) {
          console.log(err);
      });

}

function processCustomers() {
  let urlFalg = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  let JSONCustomers = JSON.parse(customersObteined);
  //alert(JSONProductos.value[0].ProductName);
  let divTableCustomers = document.getElementById("divTableCustomers");
  let tableCustomer     = document.createElement("table");
  let tbody             = document.createElement("tbody");

  tableCustomer.classList.add("table");
  tableCustomer.classList.add("table-striped");

  for (var i = 0; i < JSONCustomers.value.length; i++) {

    let newRow = document.createElement("tr");

    let columnContactName = document.createElement("td");
    columnContactName.innerText = JSONCustomers.value[i].ContactName;

    let columnCity = document.createElement("td");
    columnCity.innerText = JSONCustomers.value[i].City;

    let columnFlag = document.createElement("td");
    let imgFlag =  document.createElement("img");
    imgFlag.classList.add("flag");

    if (JSONCustomers.value[i].Country == "UK") {
      imgFlag.src = urlFalg + "United-Kingdom.png";
    }else{
      imgFlag.src = urlFalg + JSONCustomers.value[i].Country + ".png";
    }

    columnFlag.appendChild(imgFlag);


    newRow.appendChild(columnContactName);
    newRow.appendChild(columnCity);
    newRow.appendChild(columnFlag);

    tbody.appendChild(newRow);

  }

  tableCustomer.appendChild(tbody);
  divTableCustomers.appendChild(tableCustomer);

}
